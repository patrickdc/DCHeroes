<?php 
$heroesBoolPvlt = false;
$teamsBoolPvlt = false;
include ("db.php") 
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Marvel Universe</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/rating.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <script src="main.js"></script>
</head>
<body>
    <div id="ContainerDivPvlt">
        <header id="LogoHeaderPvlt">
            <div id="HeaderDivPvlt">
                <div id="LogoDivPvlt"><a href="index.php"><img src="images/header.png"></a></div>
            </div>
        </header>
        <div id="ContentDivPvlt">
            <div id="LeftDivPvlt">
                <div id="NavDivPvlt">
                    <h3 id="TeamsHeaderPvlt">Teams</h3>
                    <nav id="NavPvlt">
                        <ul>
                        <?php foreach ($teamsArray as $team) { ?> 
                            <li><img class="TeamsImgPvlt" src="<?php echo $team['teamImage'];?>"><?php echo " ";?><a href="?teamId=<?php echo $team['teamId']?>"><?php echo $team['teamName']." (".$team["heroCount"].")";?></a></li>
                        <?php } ?>
                        </ul>
                    </nav>
                </div>
            </div>
            <div id="MiddleDivPvlt">
                <?php if ($teamsBoolPvlt == true) { foreach ($heroesArray as $hero) { ?>
                    <div class="HeroDivPvlt">
                        <div class="HeroImgPvlt"><img src="<?php echo $hero['heroImage'];?>"></div>
                        <div class="HeroDescPvlt">
                            <h3><?php echo $hero['heroName']." ";?></h3>
                            <p><?php echo $hero['heroDescription'];?></p>
                            <a href="?teamId=<?php echo $hero['teamId'];?>&heroId=<?php echo $hero['heroId'];?>"> <button id="MoreInfoPvlt">More Info</button> </a>  
                        </div>
                    </div>
                <?php } } ?>
            </div>
            <div id="RightDivPvlt">
                <?php if ($heroesBoolPvlt == true) { ?>
                    <div id="RightContentPvlt">
                        <div id="RatingImgDivPvlt">
                            <img id="RatingImgPvlt" src="<?php echo $img['heroImage'];?>">
                            <div id="HeroHeaderPvlt">
                                <h3>
                                    <?php echo $img["heroName"]; ?>
                                </h3>
                                <div class="rate">
                                        <input type="radio" id="rating10js" name="rating" value="10" disabled/><label class="lblRating" for="rating10"
                                            ></label>
                                        <input type="radio" id="rating9js" name="rating" value="9" disabled/><label class="lblRating half" for="rating9"
                                            ></label>
                                        <input type="radio" id="rating8js" name="rating" value="8" disabled/><label class="lblRating" for="rating8"
                                            ></label>
                                        <input type="radio" id="rating7js" name="rating" value="7" disabled/><label class="lblRating half" for="rating7"
                                            ></label>
                                        <input type="radio" id="rating6js" name="rating" value="6" disabled/><label class="lblRating" for="rating6"
                                            ></label>
                                        <input type="radio" id="rating5js" name="rating" value="5" disabled/><label class="lblRating half" for="rating5"
                                            ></label>
                                        <input type="radio" id="rating4js" name="rating" value="4" disabled/><label class="lblRating" for="rating4"
                                            ></label>
                                        <input type="radio" id="rating3js" name="rating" value="3" disabled/><label class="lblRating half" for="rating3"
                                            ></label>
                                        <input type="radio" id="rating2js" name="rating" value="2" disabled/><label class="lblRating" for="rating2"
                                            ></label>
                                        <input type="radio" id="rating1js" name="rating" value="1" disabled/><label class="lblRating half" for="rating1"
                                            ></label>
                                </div>
                            </div>
                        </div>
                        <div id="RatingContentDivPvlt">
                            <form action="<?php echo $_SERVER['PHP_SELF']; ?>?teamId=<?php echo $_GET["teamId"]; ?>&heroId=<?php echo $_GET["heroId"]; ?>" method="POST" class="frmRate">
                                <fieldset id="fieldPvlt">
                                    <div>
                                    <h2 class="RatingHeaderPvlt">Powers</h2>
                                        <p id="PowersPvlt">
                                            <?php echo $img['heroPower'];?>
                                        </p>
                                        <h2 class="RatingHeaderPvlt">Rating</h2>
                                        <div class="rate" id="ratePostPvlt">
                                            <input type="radio" id="rating10" name="rating" value="10"/><label class="lblRating" for="rating10"
                                                title="5 stars"></label>
                                            <input type="radio" id="rating9" name="rating" value="9"/><label class="lblRating half" for="rating9"
                                                title="4 1/2 stars"></label>
                                            <input type="radio" id="rating8" name="rating" value="8"/><label class="lblRating" for="rating8"
                                                title="4 stars"></label>
                                            <input type="radio" id="rating7" name="rating" value="7"/><label class="lblRating half" for="rating7"
                                                title="3 1/2 stars"></label>
                                            <input type="radio" id="rating6" name="rating" value="6"/><label class="lblRating" for="rating6"
                                                title="3 stars"></label>
                                            <input type="radio" id="rating5" name="rating" value="5"/><label class="lblRating half" for="rating5"
                                                title="2 1/2 stars"></label>
                                            <input type="radio" id="rating4" name="rating" value="4"/><label class="lblRating" for="rating4"
                                                title="2 stars"></label>
                                            <input type="radio" id="rating3" name="rating" value="3"/><label class="lblRating half" for="rating3"
                                                title="1 1/2 stars"></label>
                                            <input type="radio" id="rating2" name="rating" value="2"/><label class="lblRating" for="rating2"
                                                title="1 star"></label>
                                            <input type="radio" id="rating1" name="rating" value="1"/><label class="lblRating half" for="rating1"
                                                title="1/2 star"></label>
                                        </div>
                                        <?php
                                            if($returnMessage != "")
                                            {
                                                ?>
                                        <h3 id="ReturnMessagePvlt">
                                            <?php echo $returnMessage; ?>
                                        </h3>
                                        <?php
                                            }
                                            ?>
                                    </div>
                                    <div class="divMessage">
                                        <h3>Review</h3>
                                        <textarea name="myMessage" required></textarea>
                                    </div>
                                    <div class="divSubmit">
                                        <input type="submit" name="submitRating" value="Review!" />
                                        <input type="hidden" name="heroId" value="<?php echo $hero["heroId"]; ?>"/>
                                    </div>
                                    <h3 class="reviewTable" id="reviewCommentsPvlt"><i class="far fa-comments"></i>Comments</h3>
                                    <?php
                                    if(!empty($reviews))
                                    {
                                        // print table 
                                        echo "<table class=\"reviewTable\" id=\"reviewTablePvlt\">";
                                        foreach($reviews as $heroReview)
                                        {
                                    ?>
                                            <h3 id="ratingHeaderPvlt"><?php echo strftime("%d %B %Y",$heroReview["ratingDate"]);?></h3>
                                            <h3 id="ratingHeaderTimePvlt"><?php echo strftime("%H:%M:%S",$heroReview["ratingDate"]);?></h3>
                                            <h3 id="ratingCommentPvlt"><?php echo nl2br($heroReview['ratingReview']);?></h3>
                                            <div class="linePvlt"></div>
                                    <?php 
                                    }
                                        echo "</table>";
                                    }
                                    else
                                    {
                                    ?>
                                        <h5 class="reviewTable"><i class="fas fa-info-circle"></i>No comments yet..</h5>
                                        <?php
                                    }
                                    ?>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</body>
</html>

<script>
var heroRating = "<?php echo $flooredrating;?>";
var ratingButton = heroRating * 2;
console.log (heroRating);

function check() 
{
    document.getElementById("rating" + ratingButton + "js").checked = true;

}

function uncheck() {
    document.getElementById("red").checked = false;
}

check();
</script>