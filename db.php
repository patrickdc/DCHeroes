<?php

// Create connection
$dbservernamePvlt = "localhost:3306";
$dbusername = "root";
$dbpassword = "";
$dbname = "dc-heroes";
$dbconn = mysqli_connect($dbservernamePvlt, $dbusername, $dbpassword, $dbname);
// Check connection
if (!$dbconn) 
{
    die("Connection failed: " . mysqli_connect_error());
}

$teamsArray = []; // array
$teamssqlqueryPvlt = "SELECT team.*,COUNT(hero.heroid) as 'heroCount' FROM `team` NATURAL JOIN `hero` GROUP BY team.teamId";
$result = mysqli_query($dbconn, $teamssqlqueryPvlt);

if (mysqli_num_rows($result) > 0)  //if theres more than 0 row, execute inside
{
    while($row = mysqli_fetch_assoc($result)) //while uitvoeren als row een nieuw resultaat krijgt
    {
        $teamsArray[] = $row;
    }


}

if(isset($_GET['teamId']))
{
    $heroesArray = [];
    $heroesSQL = "SELECT * FROM `hero` WHERE `teamId`= ".$_GET['teamId'];
    $resultheroes = mysqli_query($dbconn, $heroesSQL);

    if (mysqli_num_rows($resultheroes) > 0)  //if theres more than 0 row, execute inside
    {
        while($row = mysqli_fetch_assoc($resultheroes)) //while uitvoeren als row een nieuw resultaat krijgt
        {
            $heroesArray[] = $row;
        }
    }
}

if(isset($_GET['teamId']))
{
    $teamsBoolPvlt = true;
    
    if(isset($_GET['heroId']))
    {
        $heroesBoolPvlt = true;
                
        function myDump($var)
        {
            echo "<pre>";
            var_dump($var);
            echo "</pre>";
        }
        $returnMessage = "";

        // START POST SECTION
        if($_SERVER['REQUEST_METHOD'] == "POST")
        {
            if(empty($_POST['rating']) || empty($_POST['myMessage']) )
            {
                $returnMessage = "You haven't entered all the required fields.";
            }
            else
            {		
                // define SQL string
                $heroId = $_GET['heroId'];
                $ratingHero = $_POST['rating'] / 2;
                $insertSQL = 
                "INSERT INTO
                `rating`
                (
                `ratingId`,
                `heroId`,
                `rating`,
                `ratingDate`,
                `ratingReview`
                )
                VALUES
                (
                null,
                '$heroId',
                '" . $ratingHero . "',
                " . time() . ",
                '" . $_POST['myMessage'] . "'
                )
                ";
                
                $resource = mysqli_query($dbconn, $insertSQL) or die (mysqli_error($dbconn));
                
                if(!$resource)
                {
                    $returnMessage = "<h1>Something went wrong!</h1>";
                }
                else
                {
                    $returnMessage = "You have rated " . ($ratingHero) ." stars!<br>";
                    $returnMessage .= "Rating and Review inserted in the database!";
                }
            }
        }
        // END POST SECTION
        
        // SQL to get heroes from the database
        $selectHeroesSQL = "SELECT * FROM `hero`";
        
        // heroId as parameter in the URL?
        if(isset($_GET['heroId']))
        {
            $heroId = $_GET['heroId'];
            // extend the SQL
            $selectHeroesSQL .= " WHERE `heroId` = " . $heroId;
        }
        
        // extend the SQL
        $selectHeroesSQL .= " ORDER BY RAND() LIMIT 1";
        
        // run the query / send to database server
        $resource = mysqli_query($dbconn, $selectHeroesSQL) or die (mysqli_error($dbconn));
        
        // get number of rows that matches the query
        $rowcount = mysqli_num_rows($resource);
        
        // empty array of heroes
        $heroes = array();
        
        // loop through the resource and return a row of an associative array of each hero
        if($rowcount > 0)
        {
            while($row 	= mysqli_fetch_assoc($resource))
            {
                // add heroes to the array heroes
                $hero = $row;
            }
        }
        
        // get reviews (if not empty) from hero
        $selectReviewsQuery = "SELECT * FROM `rating` WHERE `ratingReview` != '' AND `heroId` = " . $hero["heroId"];
        
        $reviews 	= array();
        $resource 	= mysqli_query($dbconn, $selectReviewsQuery) or die (mysqli_error($dbconn));
        while($row 	= mysqli_fetch_assoc($resource))
        {
            // add items to the array
            $reviews[] = $row;
        }
        
        // now get the average rating of hero
        $selectAVGQuery =
        "SELECT (AVG(`rating`)) as heroRating FROM `rating` WHERE `heroId` = " . $hero["heroId"];
        
        $resource 	= mysqli_query($dbconn, $selectAVGQuery) or die (mysqli_error($dbconn));
        $rating 	= mysqli_fetch_assoc($resource);
        
        $heroIMGget = $_GET['heroId'];
        $selectIMGhero = "SELECT heroImage,heroName,heroPower FROM `hero` WHERE `heroId` = " . $heroIMGget;
        $resource 	= mysqli_query($dbconn, $selectIMGhero) or die (mysqli_error($dbconn));
        $img 	= mysqli_fetch_assoc($resource);

        
        $ratingecho = round($rating['heroRating'],2);
        $flooredrating = floor($ratingecho * 2) / 2;
    }
}
?>