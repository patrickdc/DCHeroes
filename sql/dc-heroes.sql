-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 22, 2019 at 09:04 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dc-heroes`
--

-- --------------------------------------------------------

--
-- Table structure for table `hero`
--

CREATE TABLE `hero` (
  `heroId` int(3) NOT NULL COMMENT 'the unique heroId used as a parameter in the URL and fetched by PHP using the $_GET superblobal variable',
  `heroName` varchar(50) NOT NULL COMMENT 'the name of the hero, just a string',
  `heroDescription` text NOT NULL COMMENT 'some information of the hero, just a string',
  `heroPower` text NOT NULL,
  `heroImage` varchar(50) NOT NULL COMMENT 'the image of the hero is strored as a string. The actual image is strored on the server. Use the string as the source of the HTML img-tag.',
  `teamId` int(3) NOT NULL COMMENT 'this is the teamId. Used as a referenc to the team table.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hero`
--

INSERT INTO `hero` (`heroId`, `heroName`, `heroDescription`, `heroPower`, `heroImage`, `teamId`) VALUES
(1, 'Iron Man', 'Anthony Edward Stark is the son of wealthy industrialist and head of Stark Industries, Howard Stark, and Maria Stark. A boy genius, he enters MIT at the age of 15 to study engineering and later receives master\'s degrees in engineering and physics. After his parents are killed in a car accident, he inherits his father\'s company. ', 'Iron Man possesses powered armor that gives him superhuman strength and durability, flight, and an array of weapons. The armor is invented and worn by Stark (with occasional short-term exceptions). Other people who have assumed the Iron Man identity include Stark\'s long-time partner and best friend James Rhodes; close associates Harold \"Happy\" Hogan; Eddie March; (briefly) Michael O\'Brien and Riri Williams. ', 'images/heroes/ironman.jpg', 1),
(2, 'Thor', 'Thor\'s father Odin decides his son needed to be taught humility and consequently places Thor (without memories of godhood) into the body and memories of an existing, partially disabled human medical student, Donald Blake. After becoming a doctor, Blake witnesses the arrival of an alien scouting party while he is on vacation in Norway. Blake flees from the aliens into a cave. After discovering Thor\'s hammer Mjolnir (disguised as a walking stick) and striking it against a rock, he transforms into the thunder god. Later, in Thor #159, Blake is revealed to have always been Thor, Odin\'s enchantment having caused him to forget his history as The Thunder God and believe himself mortal.', 'Like all Asgardians, Thor is incredibly long-lived and relies upon periodic consumption of the Golden Apples of Idunn to sustain his extended lifespan, which to date has lasted many millennia. Being the son of Odin and the elder goddess Gaea, Thor is physically the strongest of the Asgardians. Thor is capable of incredible feats of strength, such as lifting the almost Earth-sized Midgard Serpent, supporting a weight equivalent to that of 20 planets, and by combining his power with that of Beta Ray Bill, destroying Surtur\'s solar system-sized dimensional portal.[216] If pressed in battle, Thor is capable of entering into a state known as the \"Warrior\'s Madness\" (\"berserkergang\" in Norwegian and Danish alike), which will temporarily increase his strength and stamina tenfold, although in this state he attacks friend and foe alike.', 'images/heroes/thor.jpg', 1),
(3, 'The Hulk', 'The original Grey Hulk was shown as average in intelligence who roamed aimlessly and became annoyed at \"puny\" humans who took him for a dangerous monster. Shortly after becoming the Hulk, his transformation continued turning him green, coinciding with him beginning to display primitive speech, and by Incredible Hulk #4, radiation treatments gave Banner\'s mind complete control of the Hulk\'s body. While Banner relished his indestructibility and power, he was quick to anger and more aggressive in his Hulk form and while he became known as a hero alongside the Avengers, his increasing paranoia caused him to leave the group, believing he would never be trusted.', 'Banner is considered one of the greatest scientific minds on Earth, possessing \"a mind so brilliant it cannot be measured on any known intelligence test.\" Norman Osborn estimates that he is the fourth most-intelligent person on Earth. Banner holds expertise in biology, chemistry, engineering, medicine, physiology, and nuclear physics. Using this knowledge, he creates advanced technology dubbed \"Bannertech\", which is on par with technological development from Tony Stark or Doctor Doom. Some of these technologies include a force field that can protect him from the attacks of Hulk-level entities, and a teleporter. ', 'images/heroes/hulk.jpg', 1),
(4, 'Black Panther', 'The Black Panther is the ceremonial title given to the chief of the Panther Tribe of the advanced African nation of Wakanda. In addition to ruling the country, he is also chief of its various tribes (collectively referred to as the Wakandas). The Panther habit is a symbol of office (head of state) and is used even during diplomatic missions. The Panther is a hereditary title, but one must still earn it. ', 'The title \"Black Panther\" is a rank of office, chieftain of the Wakandan Panther Clan. As chieftain, the Panther is entitled to consume a special heart-shaped herb which, in addition to his mystical, shamanistic connection with the Wakandan Panther God Bast, grants him superhumanly acute senses, enhanced strength, speed, agility, stamina, durability, healing, and reflexes.', 'images/heroes/blackpanther.jpg', 1),
(5, 'Spiderman', 'As one contemporaneous journalist observed, \"Spider-Man has a terrible identity problem, a marked inferiority complex, and a fear of women. He is anti-social, [sic] castration-ridden, racked with Oedipal guilt, and accident-prone ... [a] functioning neurotic\". Agonizing over his choices, always attempting to do right, he is nonetheless viewed with suspicion by the authorities, who seem unsure as to whether he is a helpful vigilante or a clever criminal.', 'A bite from a radioactive spider triggers mutations in Peter Parker\'s body, granting him superpowers. In the original Lee-Ditko stories, Spider-Man has the ability to cling to walls, superhuman strength, a sixth sense (\"spider-sense\") that alerts him to danger, perfect balance and equilibrium, as well as superhuman speed and agility. The character was originally conceived by Stan Lee and Steve Ditko as intellectually gifted, but later writers have depicted his intellect at genius level. Academically brilliant, Parker has expertise in the fields of applied science, chemistry, physics, biology, engineering, mathematics, and mechanics. With his talents, he sews his own costume to conceal his identity, and he constructs many devices that complement his powers, most notably mechanical web-shooters to help navigate and trap his enemies along with a spider-signal as a flashlight and a warning beacon to criminals.', 'images/heroes/spiderman.jpg', 1),
(6, 'Ronan the Accuser', 'Ronan was born on the planet Hala, the capital of the Kree Empire in the Greater Magellanic Cloud. Ronan later joined the Accuser Corps, who are the Kree equivalent of military governors and jurists, and his rise through their ranks was extraordinary; he eventually became the third-most powerful being in the Kree Empire. The Supreme Intelligence ultimately appointed him \"Supreme Accuser of the Kree Empire\", as which he is known simply as \"Ronan the Accuser\". ', 'As a Kree warrior in peak physical condition, Ronan possesses his species\' unique physiology and is thus resistant to poisons, toxins, and diseases. He has superhuman physical attributes which are all further enhanced by his exoskeleton body-armor. Ronan\'s armor, which contains multiple scanners, can also create fields of invisibility, and his gauntlets can discharge cosmic energy bolts or generate sufficient coldness to place certain lifeforms into a state of suspended animation. In addition, Ronan possesses a brilliant strategic mind; his high intellect allowed him to rise to one of the highest positions in the Kree Empire. He also has extensive knowledge in all matters pertaining to Kree law and is well-versed in the use of his species\' highly advanced technology. ', 'images/heroes/ronan.png', 2),
(7, 'Supreme Intelligence', 'The Supreme Intelligence, also known by the honorific title Supremor, was created more than a million years ago on the planet Kree-Lar by the extraterrestrial race known as the Kree to help them create a Cosmic Cube like their enemies the Skrulls had once created. The Supreme Intelligence is composed of the brains of the greatest Kree minds (namely the Kree\'s thinkers, generals, philosophers, scientists, and so on), removed upon their deaths and assimilated by the computer, adding their knowledge and experience to its own. ', 'The Kree Supreme Intelligence is a vast cybernetic/organic computer system composed of 5,000 cubic meters of computer circuitry incorporating the disembodied brains of the greatest statesmen and philosophers in Kree history, preserved cryogenically. This aggregation of brains creates a single collective intelligence able to use the vast information storage and processing capabilities of the computer system in a creative way. When wishing to interact with it, the Kree address it within its terminal chamber, where a holographic image is projected on a gigantic monitor screen. ', 'images/heroes/supreme.jpg', 2),
(8, 'Korath the Pursuer', 'Korath-Thak is an agent of the Kree Empire. He was a cyber-geneticist, and the founder and head of the Pursuer Project to develop cybernetic warriors for the Kree militia. He has also been a munitions manufacturing plant foreman, and a special operative of the Supreme Intelligence. ', 'Korath is a member of the alien Kree race, who was augmented by an unknown experimental cyber-genetic engineering process. He has superhuman strength, stamina, and durability. He also has the ability to psionically locate individuals by tracing their brain patterns. Like other Kree, Korath is unable to breathe in Earth\'s atmosphere without a special apparatus or breathing serum. ', 'images/heroes/korath.webp', 2),
(9, 'Karla Sofen', 'Karla Sofen was born in Van Nuys, California. She grew up in the mansion of Hollywood producer Charles Stockbridge, as the child of the butler Karl Sofen. After her father\'s death, her mother Marion Sofen worked three jobs to put her daughter through college, and Karla vowed never to end up like her mother and that she would never put another\'s needs before her own. Despite building a successful practice as a psychologist and psychiatrist, Karla so disliked being dependent on her patients for income that she entered the super-criminal world as an aide to Doctor Faustus.[5] Learning of Lloyd Bloch (aka the original Moonstone), she became the supervillain\'s psychologist and manipulated him into rejecting the source of his powers, an extraterrestrial gem of considerable power, which she then acquired and absorbed to gain the powers of Moonstone. She then battled the Hulk.[6]', 'Moonstone\'s powers are derived from a Kree gravity stone (found on Earth\'s moon) which is charged with unknown energy and bonded to her nervous system.\r\n\r\nShe can use the stone to fly, and to become intangible so as to pass through solid objects (while intangible she is immune even to some magic-based attacks[citation needed]). She can project laser-like energy beams from her hands (able to penetrate steel plates), and has also shown the ability to discharge non-coherent light omni-directionally in a blinding flash. Additionally, her bond with the stone grants her superhuman strength, stamina, speed and reflexes, as well as sufficiently enhanced healing to even recover from a broken neck with enough time. ', 'images/heroes/karlasofen.jpg', 2),
(10, 'Doctor Minerva', 'Minn-Erva was born in Edelix, on the planet Kree-Lar. She became a geneticist and an agent of the Kree Empire. She was stationed on the Kree science cruiser, Ananim. She orbited the Earth in the starship, observing Mar-Vell. She then abducted Rick Jones, and lured Mar-Vell aboard the craft. She revealed to Mar-Vell her theory that the two of them would produce superior offspring capable of advancing the evolutionary potential of the Kree species. She was ordered by the science council head Phae-Dor to abandon her mission, but refused and was neurologically overpowered. She was rescued from the wrecked science cruiser by Mar-Vell. She later rendezvoused with the Kree scientist Mac-Ronn and docile Ronan the Accuser on a farm in Sullivan County, Texas. Later, she observed Ronan revive and escape custody.', 'Minn-Erva is a member of the alien Kree race, and was also mutagenically altered by the Kree Psyche-Magnetron, giving her superhuman strength and durability and the power of flight through the conscious manipulation of gravitons. The machine is capable of using \"nega-energy\" for various purposes, and was set to replicate the powers of Carol Danvers. She also possesses heightened intuitive faculties enabling her to guess correctly significantly higher than chance. ', 'images/heroes/minerva.jpg', 2),
(11, 'Daisy Johnson', 'Daisy Johnson is a superhuman with seismic (earthquake-producing) powers, and is the illegitimate daughter of Calvin Zabo, the supervillain known as Mister Hyde. Taken in by S.H.I.E.L.D., she is under the careful eye of its longtime executive director, Nick Fury, even after the latter\'s defection from the agency during the events of the Secret War series. Daisy herself was a participant in this incident, where Fury used trickery, lies and outright brainwashing in order to secure a superhero team to overthrow the legitimate government of Latveria. This later resulted in a terror attack on American soil; Daisy destroys the cyborg leader.[5] She possesses a \"Level 10\" security clearance, the only known agent aside from Fury and the Black Widow (Natasha Romanova) to do so. ', 'Daisy Johnson generates powerful waves of vibrations which can produce effects resembling those of earthquakes. She is immune to any harmful effects of the vibrations. She also has or was given a form of psychic shielding.[volume & issue needed]\r\n\r\nShe is also a superb hand-to-hand combatant, skilled all-around athlete, and excellent marksman. She was a leading espionage agent, adept at undercover assignments.', 'images/heroes/daisy.jpg', 3),
(12, 'Phil Coulson', 'Phillip J. \"Phil\" Coulson is a former Director of S.H.I.E.L.D. Prior to his appointment, he served as one of the organization\'s best operatives. He acted as former Director Nick Fury\'s right-hand man for many important missions.', 'Coulson was tasked from S.H.I.E.L.D. to retrieve an unknown object classed as an 0-8-4 from the Burkov Mining Facility in Russia. Upon arrival, Coulson impersonated an agent of the United Nations in order to trying to enter on the site and retrieve the object. However, Melinda May knocked out the guard and surprised Coulson.', 'images/heroes/phil.webp', 3),
(13, 'Melinda May', 'Melinda Qiaolian May is a former Level Red S.H.I.E.L.D. operative, nicknamed \"The Cavalry\". A veteran pilot and soldier with years of experience and a good friend of Agent Phil Coulson, she withdrew from field duty after an incident in Manama, Bahrain which left her mentally damaged.', 'As Coulson\'s unofficial deputy, May helped Coulson in fighting the forces of HYDRA led by Daniel Whitehall and dealing with Coulson\'s growing obsession with the alien writing. After the Battle for the Kree City in which HYDRA was dealt a heavy blow and resulted in Agent Skye gaining superhuman abilities, May helped them all to deal with the situation the best they could. ', 'images/heroes/melinda.webp', 3),
(14, 'Al MacKenzie', 'Al MacKenzie was born in Austin, Texas. He was once the C.I.A. liaison to S.H.I.E.L.D. He became romantically involved with Contessa Valentina Allegra di Fontaine after a while, which led to an estrangement between him and Nick Fury. Because of this, he returned to the CIA with the Contessa.', 'Alphonso \"Mack\" Mackenzie is the Director of S.H.I.E.L.D. Despite the heavy casualties S.H.I.E.L.D. took during HYDRA\'s attack on it, he stayed loyal to his oath and continued his service in the remains of S.H.I.E.L.D. under Robert Gonzales. ', 'images/heroes/almac.webp', 3),
(15, 'Lance Hunter', 'After Tod Radcliffe, a Special Tactical Reserve for International Key Emergencies traitor secretly working for the Red Skull, was exposed, Commander Lance Hunter introduced himself as the Director of S.T.R.I.K.E. to Nick Fury. The agents of S.T.R.I.K.E. and S.H.I.E.L.D. worked together to track down the Red Skull\'s Nazi activities.', 'Lance Hunter has years of Naval training, with an expertise in munitions, as well as experience in espionage from working for British Intelligence.', 'images/heroes/lance.webp', 3),
(16, 'Gamora', 'Gamora is the last of her species, the Zen-Whoberis, who were exterminated by the Badoon (in her original timeline, her species was exterminated by the Universal Church of Truth). Thanos found her as a child and decided to use her as a weapon. Gamora was raised and trained by Thanos to assassinate the Magus, the evil, future version of Adam Warlock.', 'Gamora received treatments from Thanos that enhanced her speed, strength, agility, and durability to rival Adam Warlock\'s (to better slay the Magus, his evil, future self). Thanos also helped her become a formidable hand-to-hand combatant, trained in the martial-arts techniques from various planets, in the uses of the known weaponry of the Milky Way Galaxy, and stealth techniques.', 'images/heroes/gamora.webp', 4),
(17, 'Nebula', 'A brutal space pirate and mercenary, Nebula seized control of Sanctuary II, a massive spaceship previously under the command of Thanos. Thanos was believed to be dead at this point, and Nebula claimed that he had been her grandfather. Nebula\'s band of mercenaries and pirates consisted of Skunge, Kehl, Gunthar and Levan. ', 'Nebula is an athletic woman, and an excellent armed and unarmed combatant. She possesses a gifted intellect and is a brilliant battle strategist.\r\n\r\nNebula uses blasters worn on her wrists that fire concussive blasts of unknown energy or heat blasts that can incinerate a human being almost instantly. She also apparently wears a device that enables her to disguise her appearance, either through illusion-casting or through actual molecular rearrangement of her body and clothing. ', 'images/heroes/nebula.webp', 4),
(18, 'Ebony Maw', 'Ebony Maw was a member of Thanos\' Black Order. He is not a fighter, but a dangerous thinker of the Black Order. When Thanos targeted Earth as the next planet he would raze during the Infinity storyline, Ebony Maw was dispatched to deal with Doctor Strange.', 'Ebony Maw has genius-level intellect. He mostly demonstrates his intelligence with his highly skilled abilities of manipulation being described as a \"black tongue that spreads mischief and evil wherever he goes.\" Ebony Maw’s skills at manipulation are a result of his superhumanly persuasive voice which allows him to control even the strongest minds such as Doctor Strange. ', 'images/heroes/ebony.jpg', 4),
(19, 'Black Dwarf', 'Black Dwarf is a member of Thanos\' Black Order where he is the powerhouse of the Mad Titan\'s army.\r\n\r\nWhen Thanos targeted Earth as the next planet he would raze during the Infinity, Black Dwarf arrived in Wakanda. To his surprise, Black Dwarf found great resistance in that country and was forced to retreat. For his failure, Thanos expelled Black Dwarf from the Black Order.', 'Black Dwarf has super-strength, enhanced density, and his skin is impenetrable. ', 'images/heroes/blackdwarf.jpg', 4),
(20, 'Proxima Midnight', 'Proxima Midnight is a member of Thanos\' Black Order. Thanos chose her for her expert combatant skills. Proxima is the wife of fellow Order member Corvus Glaive. She was sent to Earth to retrieve an Infinity Gem from Namor, but came into contact with the New Avengers. She battled Spectrum and Luke Cage where she found herself evenly matched.', 'Proxima possessed the typical attributes of a super powered individual including super strength, super speed, super endurance and some invulnerability. Being able to survive planetary reentry without injury, she was also a master combatant and owned a spear created from a star trapped in a quantum singularity by Thanos himself.', 'images/heroes/proxima.jpg', 4),
(21, 'Star-Lord', 'When J\'son\'s ship crash lands on Earth, he is taken in by Meredith Quill. The two form a relationship while J\'son makes repairs to his ship. Eventually, J\'son is forced to leave to return home and fight in a war. He leaves, not knowing Meredith is pregnant with Peter Quill.', 'Star-Lord is a master strategist and problem solver who is an expert in close-quarter combat, various human and alien firearms, and battle techniques. He has extensive knowledge of various alien customs, societies, and cultures, and considerable knowledge about cosmic abstracts, such as Oblivion.\r\n', 'images/heroes/starlord.jpg', 5),
(22, 'Rocket', 'Rocket Raccoon acts as the \"Guardian of the Keystone Quadrant\", an area of outer space sealed off from the rest of the cosmos by the so-called Galacian Wall. Rocket is captain of the starship Rack \'n\' Ruin, and he and his first mate Wal Rus (a talking walrus) come from the planet Halfworld in the Keystone Quadrant, an abandoned colony for the mentally ill where the animal companions were genetically manipulated to grant them human level intelligence and bipedal body construction for many to become caretakers of the inmates. Rocket was Halfworld\'s chief law officer (\"ranger\") who protected the colony against various threats.[17]', 'Rocket Raccoon possesses the normal attributes of an Earth raccoon, including speed (which has been additionally amplified by his training), and an acute sense of smell, sight, hearing and touch. Sharp claws allow him to scale walls, buildings, and trees with ease. ', 'images/heroes/rocket.jpg', 5),
(23, 'Groot', 'Groot is an extraterrestrial tree monster who initially came to Earth seeking humans to capture and study. Groot was seemingly destroyed by termites used by Leslie Evans.\r\n\r\nXemnu made a duplicate of Groot by making a human and tree hybrid that was used to fight the Hulk, but it was destroyed in the battle.', 'Groot can absorb wood as food, and has the ability to regenerate. Groot can control trees and plants, using them to attack others, and appears to be resistant to fire. Groot is able to sprout dramatically increasing mass which then severely inhibits movement.\r\n\r\nGroot has been seemingly killed on multiple occasions, each time regrowing from a sprig.', 'images/heroes/groot.png', 5),
(24, 'Drax', 'While driving through a desert with his wife and daughter, Arthur Douglas\' car is attacked by a spaceship piloted by Thanos, who thinks the humans have seen him. His daughter, Heather, survives the crash and is adopted by Thanos\' father, Mentor, and raised on Titan. She later becomes Moondragon.', 'Drax\'s initial incarnation\'s powers included superhuman strength, stamina and resistance to physical injury as well the ability to project concussive blasts of cosmic energy from his hands. He could also travel at high speeds in outer space and hyperspace without air, food, or water. Drax also had telepathy, having used it to engage Thanos in a psychic battle and stalemating him mind to mind.', 'images/heroes/drax.jpg', 5),
(25, 'Gamora', 'Gamora is the last of her species, the Zen-Whoberis, who were exterminated by the Badoon (in her original timeline, her species was exterminated by the Universal Church of Truth). Thanos found her as a child and decided to use her as a weapon. Gamora was raised and trained by Thanos to assassinate the Magus, the evil, future version of Adam Warlock.', 'Gamora received treatments from Thanos that enhanced her speed, strength, agility, and durability to rival Adam Warlock\'s (to better slay the Magus, his evil, future self). Thanos also helped her become a formidable hand-to-hand combatant, trained in the martial-arts techniques from various planets, in the uses of the known weaponry of the Milky Way Galaxy, and stealth techniques.', 'images/heroes/gamora.webp', 5);

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `ratingId` int(3) NOT NULL COMMENT 'unique rating is, auto incremented',
  `heroId` int(3) NOT NULL COMMENT 'the heroId used as reference to the hero table, can''t be unique in thie table',
  `rating` float NOT NULL COMMENT 'rating is defined as an integer from 0 (min) to 10 (max)',
  `ratingDate` int(5) NOT NULL COMMENT 'the date of this rating. Dates are presented as an integer (timestamp) and displayed as a human readable date and time string using the PHP strftime() function',
  `ratingReview` text NOT NULL COMMENT 'a textual review added by the user\\nthe form where the user can rate the hero by using stars (radio-buttons)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`ratingId`, `heroId`, `rating`, `ratingDate`, `ratingReview`) VALUES
(64, 1, 2, 1550655332, '1.5'),
(65, 1, 2.5, 1550655392, '2.5'),
(66, 1, 2.5, 1550655459, '2.5'),
(67, 2, 1.5, 1550655477, '1.5'),
(68, 5, 0.5, 1550664390, '0.5 star'),
(69, 5, 1.5, 1550664404, '1.5 star'),
(70, 4, 2, 1550738365, '2 stars'),
(71, 4, 3, 1550738372, '3 stars');

-- --------------------------------------------------------

--
-- Table structure for table `rating_new`
--

CREATE TABLE `rating_new` (
  `ratingId` int(11) NOT NULL,
  `heroId` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `ratingReview` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `teamId` int(3) NOT NULL COMMENT 'unique teamId can be used as a parameter in the URL and fetched using the $_GET variable',
  `teamName` varchar(50) NOT NULL COMMENT 'team name, just an ordinary string',
  `teamDescription` text NOT NULL COMMENT 'team description, just a string',
  `teamImage` varchar(100) NOT NULL COMMENT 'team image, stored as a string and used with the source of the HTML-tag'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`teamId`, `teamName`, `teamDescription`, `teamImage`) VALUES
(1, 'The Avengers', '', 'images/teams/avengers.png'),
(2, 'Kree', '', 'images/teams/kree.png'),
(3, 'S.H.I.E.L.D.', '', 'images/teams/shield.jpg'),
(4, 'Children of Thanos', '', 'images/teams/thanos.png'),
(5, 'Guardians of the Galaxy', '', 'images/teams/guardians.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hero`
--
ALTER TABLE `hero`
  ADD PRIMARY KEY (`heroId`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`ratingId`);

--
-- Indexes for table `rating_new`
--
ALTER TABLE `rating_new`
  ADD PRIMARY KEY (`ratingId`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`teamId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hero`
--
ALTER TABLE `hero`
  MODIFY `heroId` int(3) NOT NULL AUTO_INCREMENT COMMENT 'the unique heroId used as a parameter in the URL and fetched by PHP using the $_GET superblobal variable', AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `ratingId` int(3) NOT NULL AUTO_INCREMENT COMMENT 'unique rating is, auto incremented', AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `rating_new`
--
ALTER TABLE `rating_new`
  MODIFY `ratingId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `teamId` int(3) NOT NULL AUTO_INCREMENT COMMENT 'unique teamId can be used as a parameter in the URL and fetched using the $_GET variable', AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
